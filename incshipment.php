<table border="0" cellpadding="5" cellspacing="0" width="650">
      <tr>
        <td align="left" valign="top" class="page_content">
		<font face="Times New Roman, Times, serif" size="3">+ Cty TNHH TM DV <b><u><font color="#006600">Thiên Thần Nhỏ</font></u> .</b><br><hr />
		<b>+ <u>Trụ sở</u></b>:  237 Nguyễn Văn Cừ, quận 1, Sài Gòn .<br><b>+ <u>Chi Nhánh</u></b> : 468/17 Nguyễn Tri Phương Phường 9 , Quận 10 ,Tp.HCM .<br><b>+ <u>Kinh doanh</u></b> : (08) 22.00.6666 - 22.100.100 .<br><b>+ <u>Kĩ thuật - Bảo hành</u></b> : (08) 62.738.759 .<br><b>+ <u>Thời gian làm việc</u> : </b><br>- Thứ 2 đến thứ 7 : Sáng 8h =&gt; 11h30 - Trưa 13h đến 19h. <br>- Chủ nhật : 9h =&gt; 16h .<br><b>+ <u>Thời gian bảo hành</u> : </b><br>   - Thứ 2 đến thứ 7 : 13h =&gt; 18h . <br>   - Chủ nhật - ngày lễ không nhận bảo hành .<br><br><hr /><b>- <u>Phương thức thanh toán 1</u> : <font color="red">Thanh toán trực tiếp tại BabyShop</font></b> .<br><br>+ Quý khách ở Tp.HCM đến mua hàng và trả tiền trực tiếp tại BabyShop .<br><br></font><font face="Times New Roman, Times, serif" size="3"><b>- <u>Phương thức thanh toán 2</u> : <font color="red">Thanh toán qua ngân hàng</font> .</b><br><br>
        +Quý khách đến bất kì ngân hàng nào ở Việt Nam để chuyển tiền theo 
thông  tin bên dưới ( Quý khách nên chọn các chi nhánh của Ngân hàng 
cùng hệ thống để không mất phí chuyển khoản và nhận tiền  rất nhanh , 
chỉ khoảng 5 phút )</font><font size="3"><br><br></font><font face="Times New Roman, Times, serif" size="3">+ Ngân hàng Ngoại Thương Việt Nam ( VietComBank ) chi nhánh Bình Tây .<br>
+ Tên tài khoản : Mr.X .<br>
+ Số tài khoản : 0251.0014.03245 .<br>
<br></font><font face="Times" size="3">+ Ngân hàng Thương Mại Á Châu ( ACB ) chi nhánh 3 tháng 2 .<br>
+ Tên tài khoản : Trương Tấn Sang.<br>
+ Số tài khoản : 1139.55123 .</font><font face="Times New Roman, Times, serif" size="3"><br>
<br><b>- <u>Phương thức thanh toán 3</u> : <font color="red">Thanh toán qua ATM và online </font>.</b><br><br>+
  Quý khách ra bất kì trạm ATM nào của Vietcombank trên toàn lãnh thổ  
Việt Nam hoặc sử dụng dịch vụ chuyển khoản online đều có  thể chuyển 
khoản vào số tài khoản sau :<br><br>+ Ngân hàng Ngoại Thương Việt Nam ( VietComBank ) chi nhánh Bình Tây .<br>
+ Tên tài khoản : Nguyễn Phú Trọng.<br>+ Số tài khoản : 0251.0014.07389 .<font size="3"><br><br></font><font face="Times New Roman, Times, serif" size="3">+ Ngân hàng Thương Mại Á Châu ( ACB ) chi nhánh 3 tháng 2 .<br>
+ Tên tài khoản : Nguyễn Sinh Hùng.<br>+ Số tài khoản : 1139.55499 .<br><br>+ Khi chuyển tiền , quý khách vui lòng ghi rõ tên và lí do chuyển tiền . VD : Nguyễn A chuyển tiền mua máy tính .<br><br>
</font><font face="Times New Roman, Times, serif" size="3">+
  Khi chuyển tiền xong , quý khách vui lòng thông báo cho chúng tôi tên 
 hoặc số tài khoản đã chuyển và địa chỉ nhận hàng để chúng tôi tiến hành
 kiểm tra  tiền và chuyển hàng cho quý khách . Hàng hóa chuyển phát theo
 phương  thức chuyển phát nhanh hoặc chậm theo yêu cầu . Nếu ở những 
tỉnh  thành lớn , quý khách có thể thanh toán phí vận chuyển trực tiếp 
với  nhân viên chuyển phát . Còn ở những tỉnh thành khác quý khách vui 
lòng  thanh toán luôn phần phí vận chuyển khi chuyển tiền mua hàng .<br><hr /></font></td>
      </tr>
</table>