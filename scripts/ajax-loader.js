$(document).ready(function(){
	$("#bt_signup").click(function() {
		// variables check form
		var fullname = $("#fullname").val();		
		var birthday = $("#BirthDay").val() + $("#BirthMonth").val() + $("#BirthYear").val();
		var location = $("#location").val();
		var username = $("#name").val();
		var password = $("#pass_main").val();
		var pass_temp = $("#pass_temp").val();
		var captcha = $("#captcha").val();
		
		// variable check username availability
		var min_char = 5;
		var char_error = '<img src="images/logos/error.png" />';
		
		// check fullname
		if (fullname == "" || fullname.length < 6) {
			$(".nameFail").fadeIn('slow').delay(2000).fadeOut('fast');
            $("#fullname").focus();
            return false;
		}
		
		// check birthday
		if (birthday.length < 6 ) {
			$(".dateFail").fadeIn('slow').delay(2000).fadeOut('fast');
            return false;
		}
		
		// check location
		if (location == "") {
			$(".locationFail").fadeIn('slow').delay(2000).fadeOut('fast');          
            return false;
		}
		
		// check username
		if (username.length < min_char) {
			//$('#check_username_result').html(char_error);
			$(".usernameFail").fadeIn('slow').delay(2000).fadeOut('fast');
			return false;
		} else {
			//$('#check_username_result').html(char_error);
			check_username();
			//return false;
		}
		
		// check password
		if (password == "" || password.length < 5) {
			$(".passFail").fadeIn('slow').delay(2000).fadeOut('fast');
            $("#pass_main").focus();
            return false;
		}
		
		// check password again
		if ((pass_temp == "") || (pass_temp != password)) {
			$(".passFail").fadeIn('slow').delay(2000).fadeOut('fast');
            $("#pass_temp").focus();
            return false;
		}
		
		// check captcha
		if (captcha == "") {
			$(".captchaFail").fadeIn('slow').delay(2000).fadeOut('fast');
            $("#captcha").focus();
            return false;
		}  
		
		return true;
		
	});
	
	// function to check username
	function check_username() {
		// get username
		var username = $('#name').val();
		// ajax
		$.post("check_username.php", { username: username },
		function(result) {
			if(result == 1) {
				$(".usernameSuccess").fadeIn('slow').delay(2000).fadeOut('fast');
			} else {
				$(".usernameFail").fadeIn('slow').delay(2000).fadeOut('fast');
				$("#name").focus();
				$("#name").val("");
			}
		});
	}
	
	// event load captcha
	$("#load_captcha").click(function() {
        change_captcha();
	});
	// function change captcha
	function change_captcha() {
        document.getElementById("img_captcha").src="captcha.php?rnd=" + Math.random();
	}
});