<?php
function StyledPanel($title,$call,$args)
{
?>
    <div class="styled-panel">
        <div class="styled-panel header">
            <h3>
            <?php echo $title ?>
            </h3>
        </div>
        <div class="styled-panel body">
             <?php call_user_func_array($call, $args) ?>
        </div>
    </div>
<?php
}
?>

<?php
function StyledPanelN($title,$call)
{
?>
    <div class="styled-panel">
        <div class="styled-panel header">
            <h3>
            <?php echo $title ?>
            </h3>
        </div>
        <div class="styled-panel body">
             <?php include_once ("$call"); ?>
        </div>
    </div>
<?php
}
?>

