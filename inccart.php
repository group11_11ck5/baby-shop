<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="get" name="myfrm">
    <table class="table" border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <th width="180">Tên hàng</th>
        <th width="50">Số lượng</th>
        <th width="50">Đơn giá</th>
        <th width="100">Thành tiền</th>
        <th width="30">&nbsp;</th>
        <th width="30">&nbsp;</th>
    </tr>
<?php
    if(isset($_SESSION["cartname"])==true){
        if(isset($_REQUEST["delete"])){
            $_SESSION["cartname"]->DeleteProduct($_REQUEST["delete"]);
        }
        if(isset($_REQUEST["txtProductPrice"])){
            $_SESSION["cartname"]->EditProduct($_REQUEST["update"],$_REQUEST["txtProductPrice"]);
        }
        foreach ($_SESSION["cartname"]->getProductList() as $value){
?>
            <tr align="center" valign="middle">
                <td><?php echo $value->getProductName();?></td>
                <td>
<?php
                if(isset($_REQUEST["edit"])){
                    if($value->getProductID()==$_REQUEST["edit"]){
?>
                        <input name="act" type="hidden" value="4"/>
                        <input name="txtProductPrice" type="text" value="<?php echo $value->getQuantity();?>"/>
<?php
                    }
                    else{
                        echo $value->getQuantity();
                    }
                }
                else{
                    echo $value->getQuantity();
                }
?>
                </td>
                <td><?php echo number_format($value->getProductPrice());?></td>
                <td><?php echo number_format($value->getSum());?></td>
                <td>
<?php
                if(isset($_REQUEST["edit"])){
                    if($value->getProductID()==$_REQUEST["edit"]){
?>
                        <input name="update" type="hidden" value="<?php echo $value->getProductID();?>"/>
                        <input name="btnUpdate" type="submit" value="Cập nhật"/>
<?php
                    }
                    else{
?>
                        <a href="index.php?act=4&edit=<?php echo $value->getProductID();?>">Sửa</a>
<?php
                    }
                }
                else{
?>
                    <a href="index.php?act=4&edit=<?php echo $value->getProductID();?>">Sửa</a>
<?php
                }
?>
                </td>
                <td>
<?php
                if(isset($_REQUEST["edit"])){
                    if($value->getProductID()==$_REQUEST["edit"]){
?>
                        <a href="index.php?act=4">Hủy</a>
<?php
                    }
                    else{
?>
                        <a href="index.php?act=4&delete=<?php echo $value->getProductID();?>">Xóa</a>
<?php
                    }
                }
                else{
?>
                    <a href="index.php?act=4&delete=<?php echo $value->getProductID();?>">Xóa</a>
<?php
                }
?>
                </td>
            </tr>
<?php
        }
    }
?>
    </table>
    <table>
        <tr>
            <td><strong>Tổng số tiền:</strong></td>
            <td><?php if(isset($_SESSION["cartname"])==true) echo number_format($_SESSION["cartname"]->SumMoney());?></td>
        </tr>
        <tr>
            <td><strong>Tổng số hàng chọn mua:</strong></td>
            <td><?php if(isset($_SESSION["cartname"])==true) echo $_SESSION["cartname"]->SumProduct();?></td>
        </tr>
        <tr valign="middle">
            <td colspan="2" align="center"><a href="index.php?act=8&check=0" class="buyButton">Thanh toán</a></td>
        </tr>
    </table>
</form>
