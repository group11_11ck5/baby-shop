/*
Navicat MySQL Data Transfer

Source Server         : XAMPP_MYSQL
Source Server Version : 50534
Source Host           : localhost:3306
Source Database       : babyshop

Target Server Type    : MYSQL
Target Server Version : 50534
File Encoding         : 65001

Date: 2014-01-01 18:07:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `banner`
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `Banner_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Banner_Name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Banner_Url` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Banner_Pic` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Banner_ID`),
  UNIQUE KEY `pk_banner` (`Banner_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES ('1', 'Beach Queen', 'http://www.1999.co.jp/search_e.asp?Typ1_c=101&andor=0&scope=0&scope2=0&sortID=0&urikire=1&ItKey=figure%20WAVE', 'images/Banner/Banner01.jpg');
INSERT INTO `banner` VALUES ('2', 'Nen', 'http://www.goodsmile.info/en/product/4141/', 'images/Banner/Banner02.jpg');
INSERT INTO `banner` VALUES ('3', 'Idol Master', 'http://www.goodsmile.info/en/product/4151/GSR+Character+Customize+Series+Decals+047+Puchimasu.html', 'images/Banner/Banner03.jpg');
INSERT INTO `banner` VALUES ('4', 'Figma', 'http://www.figma.jp/', 'images/Banner/Banner04.gif');
INSERT INTO `banner` VALUES ('5', 'Se-Kirara', 'http://www.se-kirara.jp/', 'images/Banner/Banner05.jpg');
INSERT INTO `banner` VALUES ('6', 'Max Factory', 'http://maxfactory.cms.drecom.jp/www.figma.jp/products/2013/fig13book-01.html', 'images/Banner/Banner06.jpg');
INSERT INTO `banner` VALUES ('7', 'Round', 'http://event.goodsmile.info/event/event-1176/?lo=en-us', 'images/Banner/Banner07.jpg');
INSERT INTO `banner` VALUES ('8', '30th', 'http://amiamiblog.com/', 'images/Banner/Banner08.jpg');

-- ----------------------------
-- Table structure for `bill`
-- ----------------------------
DROP TABLE IF EXISTS `bill`;
CREATE TABLE `bill` (
  `Bill_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Bill_User` int(64) DEFAULT NULL,
  `Bill_Date` date DEFAULT NULL,
  `Bill_Payment` double DEFAULT NULL,
  PRIMARY KEY (`Bill_ID`),
  UNIQUE KEY `pk_bill` (`Bill_ID`) USING BTREE,
  KEY `fk_bill_user` (`Bill_User`),
  CONSTRAINT `pk_User` FOREIGN KEY (`Bill_User`) REFERENCES `user` (`User_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of bill
-- ----------------------------
INSERT INTO `bill` VALUES ('1', '2', '2013-12-19', '0');
INSERT INTO `bill` VALUES ('2', '1', '2013-12-27', '0');

-- ----------------------------
-- Table structure for `billdetail`
-- ----------------------------
DROP TABLE IF EXISTS `billdetail`;
CREATE TABLE `billdetail` (
  `bDetail_ID` int(11) DEFAULT NULL,
  `bDetail_Product` int(11) DEFAULT NULL,
  `bDetail_Amount` int(11) DEFAULT NULL,
  KEY `fk_bdetail_bill` (`bDetail_ID`),
  KEY `fk_bdetail_product` (`bDetail_Product`),
  CONSTRAINT `fk_bdetail_bill` FOREIGN KEY (`bDetail_ID`) REFERENCES `bill` (`Bill_ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_bdetail_product` FOREIGN KEY (`bDetail_Product`) REFERENCES `product` (`Product_ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of billdetail
-- ----------------------------
INSERT INTO `billdetail` VALUES ('1', '2', '2');
INSERT INTO `billdetail` VALUES ('1', '4', '1');
INSERT INTO `billdetail` VALUES ('1', '3', '4');
INSERT INTO `billdetail` VALUES ('2', '13', '1');
INSERT INTO `billdetail` VALUES ('2', '14', '1');

-- ----------------------------
-- Table structure for `brand`
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand` (
  `Brand_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Brand_Name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Brand_Desc` text COLLATE utf8_unicode_ci,
  `Brand_Logo` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Brand_ID`),
  UNIQUE KEY `pk_brand` (`Brand_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES ('1', 'Good Smile Company', 'Good Smile Company.Inc là một trong những công ty hàng đầu trong lĩnh vực đồ chơi, figure tại Nhật Bản', 'images/Brand/GSC.jpg');
INSERT INTO `brand` VALUES ('2', 'Bandai', 'Bandai là một trong những tập đoàn hàng đầu trong lĩnh vực đồ chơi tại Nhật Bản', 'images/Brand/Bandai.png');
INSERT INTO `brand` VALUES ('3', 'Khác', 'Hàng hóa không rõ nguồn gốc xuất xứ, có chơi có chịu', 'images/Brand/Other.jpg');
INSERT INTO `brand` VALUES ('4', 'Wave', 'Một trong những hàng sản xuất PVC Figure nổi tiếng của Jap.', 'images/Brand/Wave.jpg');
INSERT INTO `brand` VALUES ('5', 'Max Factory', 'Một trong những hàng sản xuất PVC Figure nổi tiếng của Jap.', 'images/Brand/Max Factory.gif');
INSERT INTO `brand` VALUES ('6', 'Orchid Seed', 'Figure \"khủng\" :kimochi: :kimochi:', 'images/Brand/Orchid Seed.jpg');
INSERT INTO `brand` VALUES ('8', 'Cospa', '', 'images/Brand/Cospa.gif');
INSERT INTO `brand` VALUES ('9', 'Movic', '', 'images/Brand/Movic.gif');

-- ----------------------------
-- Table structure for `location`
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `Location_ID` int(64) NOT NULL,
  `Location_City` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`Location_ID`),
  KEY `pk_locationid` (`Location_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of location
-- ----------------------------
INSERT INTO `location` VALUES ('1', 'An Giang');
INSERT INTO `location` VALUES ('2', 'Bà Rịa - Vũng Tàu');
INSERT INTO `location` VALUES ('3', 'Bắc Giang');
INSERT INTO `location` VALUES ('4', 'Bắc Kạn');
INSERT INTO `location` VALUES ('5', 'Bạc Liêu');
INSERT INTO `location` VALUES ('6', 'Bắc Ninh');
INSERT INTO `location` VALUES ('7', 'Bến Tre');
INSERT INTO `location` VALUES ('8', 'Bình Định');
INSERT INTO `location` VALUES ('9', 'Bình Dương');
INSERT INTO `location` VALUES ('10', 'Bình Phước');
INSERT INTO `location` VALUES ('11', 'Bình Thuận');
INSERT INTO `location` VALUES ('12', 'Cà Mau');
INSERT INTO `location` VALUES ('13', 'Cao Bằng');
INSERT INTO `location` VALUES ('14', 'Đắk Lắk');
INSERT INTO `location` VALUES ('15', 'Đắk Nông');
INSERT INTO `location` VALUES ('16', 'Điện Biên');
INSERT INTO `location` VALUES ('17', 'Đồng Nai');
INSERT INTO `location` VALUES ('18', 'Đồng Tháp');
INSERT INTO `location` VALUES ('19', 'Gia Lai');
INSERT INTO `location` VALUES ('20', 'Hà Giang');
INSERT INTO `location` VALUES ('21', 'Hà Nam');
INSERT INTO `location` VALUES ('22', 'Hà Tĩnh');
INSERT INTO `location` VALUES ('23', 'Hải Dương');
INSERT INTO `location` VALUES ('24', 'Hậu Giang');
INSERT INTO `location` VALUES ('25', 'Hòa Bình');
INSERT INTO `location` VALUES ('26', 'Hưng Yên');
INSERT INTO `location` VALUES ('27', 'Khánh Hòa');
INSERT INTO `location` VALUES ('28', 'Kiên Giang');
INSERT INTO `location` VALUES ('29', 'Kon Tum');
INSERT INTO `location` VALUES ('30', 'Lai Châu');
INSERT INTO `location` VALUES ('31', 'Lâm Đồng');
INSERT INTO `location` VALUES ('32', 'Lạng Sơn');
INSERT INTO `location` VALUES ('33', 'Lào Cai');
INSERT INTO `location` VALUES ('34', 'Long An');
INSERT INTO `location` VALUES ('35', 'Nam Định');
INSERT INTO `location` VALUES ('36', 'Nghệ An');
INSERT INTO `location` VALUES ('37', 'Ninh Bình');
INSERT INTO `location` VALUES ('38', 'Ninh Thuận');
INSERT INTO `location` VALUES ('39', 'Phú Thọ');
INSERT INTO `location` VALUES ('40', 'Quảng Bình');
INSERT INTO `location` VALUES ('41', 'Quảng Nam');
INSERT INTO `location` VALUES ('42', 'Quảng Ngãi');
INSERT INTO `location` VALUES ('43', 'Quảng Ninh');
INSERT INTO `location` VALUES ('44', 'Quảng Trị');
INSERT INTO `location` VALUES ('45', 'Sóc Trăng');
INSERT INTO `location` VALUES ('46', 'Sơn La');
INSERT INTO `location` VALUES ('47', 'Tây Ninh');
INSERT INTO `location` VALUES ('48', 'Thái Bình');
INSERT INTO `location` VALUES ('49', 'Thái Nguyên');
INSERT INTO `location` VALUES ('50', 'Thanh Hóa');
INSERT INTO `location` VALUES ('51', 'Thừa Thiên Huế');
INSERT INTO `location` VALUES ('52', 'Tiền Giang');
INSERT INTO `location` VALUES ('53', 'Trà Vinh');
INSERT INTO `location` VALUES ('54', 'Tuyên Quang');
INSERT INTO `location` VALUES ('55', 'Vĩnh Long');
INSERT INTO `location` VALUES ('56', 'Vĩnh Phúc');
INSERT INTO `location` VALUES ('57', 'Yên Bái');
INSERT INTO `location` VALUES ('58', 'Phú Yên');
INSERT INTO `location` VALUES ('59', 'Hà Nội');
INSERT INTO `location` VALUES ('60', 'TP HCM');
INSERT INTO `location` VALUES ('61', 'Đà Nẵng');
INSERT INTO `location` VALUES ('62', 'Hải Phòng');
INSERT INTO `location` VALUES ('63', 'Cần Thơ');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `Product_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Product_Name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Product_Photo` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Product_Desc` text COLLATE utf8_unicode_ci,
  `Product_MadeIn` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Product_Brand` int(11) DEFAULT NULL,
  `Product_Type` int(11) DEFAULT NULL,
  `Product_Price` double DEFAULT '0',
  `Product_Stock` float DEFAULT '0',
  `Product_Sell` float DEFAULT '0',
  `Product_View` double DEFAULT '0',
  `Product_Date` date DEFAULT '2012-12-12',
  `Product_Hot` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`Product_ID`),
  UNIQUE KEY `pk_product` (`Product_ID`),
  KEY `fk_pro_brand` (`Product_Brand`),
  KEY `fk_pro_type` (`Product_Type`),
  CONSTRAINT `fk_pro_brand` FOREIGN KEY (`Product_Brand`) REFERENCES `brand` (`Brand_ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_pro_type` FOREIGN KEY (`Product_Type`) REFERENCES `type` (`Type_ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', 'Nendoroid Tomoe Mami', 'images/Product/1.jpg', 'From the popular anime \'Puella Magi Madoka Magica\' comes a Nendoroid of the kind third year student from Madoka\'s school, Mami Tomoe.', 'Japan', '1', '1', '750000', '100', '50', '1001', '2013-12-10', '1');
INSERT INTO `product` VALUES ('2', 'RGM-96X Jesta', 'images/Product/2.jpg', 'The RGM-96X Jesta is a specialized escort and special operations mobile suit featured in Mobile Suit Gundam Unicorn. The RGM-96X is based on the RGM-89 Jegan and is used by the Earth Federation. ', 'Japan', '2', '2', '300000', '50', '25', '500', '2013-12-11', '0');
INSERT INTO `product` VALUES ('3', 'Nendoroid Hachikuji Mayoi ', 'images/Product/3.jpg', 'From the popular anime series \'Bakemonogatari\' comes a Nendoroid of Mayoi Hachikuji! ', 'Japan', '1', '1', '600000', '75', '2', '889', '2013-12-12', '0');
INSERT INTO `product` VALUES ('4', 'Nendoroid Kuroki Tomoko', 'images/Product/4.jpg', 'From the anime series that many people are sure to sympathize with, \'Watashi ga Motenai no wa dou Kangaetemo Omaera ga Warui!\' comes a Nendoroid of the main character of the series, Tomoko Kuroki! ', 'Japan', '1', '1', '630000', '50', '10', '779', '2013-12-13', '0');
INSERT INTO `product` VALUES ('5', 'RX-79[G]Ez-8', 'images/Product/5.jpg', 'The RX-79[G]Ez-8 Gundam Ez8 (ガンダムEz8) is a custom variant of the original RX-79[G] Gundam Ground Type. ', 'Japan', '2', '2', '260000', '80', '30', '255', '2013-05-11', '0');
INSERT INTO `product` VALUES ('6', 'MSA-003 Nemo', 'images/Product/6.jpg', 'The MSA-003 Nemo is a mass-production attack-use mobile suit first featured in the anime series Mobile Suit Zeta Gundam. The design was later updated and featured in the light novel/OVA series Mobile Suit Gundam Unicorn. ', 'Japan', '2', '2', '220000', '50', '10', '674', '2013-02-03', '0');
INSERT INTO `product` VALUES ('7', '[Cup]01', 'images/Product/7.jpg', 'From the popular anime Toaru Kagaku no Railgun', 'VietNam', '3', '4', '80000', '100', '2', '112', '2013-04-21', '0');
INSERT INTO `product` VALUES ('8', '[Cup]02', 'images/Product/8.jpg', 'From the popular anime Shingeki no Kyojin', 'VietNam', '3', '4', '90000', '90', '40', '333', '2013-09-21', '0');
INSERT INTO `product` VALUES ('9', '[Cup]03', 'images/Product/9.jpg', 'From the popular anime Toaru Kagaku no Railgun', 'VietNam', '3', '4', '60000', '40', '10', '122', '2013-01-01', '0');
INSERT INTO `product` VALUES ('10', '[Cushion]01', 'images/Product/10.jpg', 'Miku ~desu', 'China', '3', '3', '150000', '140', '9', '9999', '2013-09-08', '0');
INSERT INTO `product` VALUES ('11', '[Cushion]02', 'images/Product/11.jpg', 'Miku ~ero', 'China', '3', '3', '180000', '10', '5', '9890', '2013-12-20', '0');
INSERT INTO `product` VALUES ('12', '[Cushion]03', 'images/Product/12.jpg', 'Miku ~loli', 'China', '3', '3', '170000', '200', '15', '10000', '2013-12-22', '0');
INSERT INTO `product` VALUES ('13', '[Cushion]04', 'images/Product/13.jpg', 'Madoka ~chan', 'China', '3', '3', '200000', '999', '88', '9090', '2013-12-09', '0');
INSERT INTO `product` VALUES ('14', 'Nendoroid Akagi', 'images/Product/14.jpg', '1st Carrier Divison Akagi, heading out!\r\n\r\nFrom \'Kantai Collection ~KanColle~\' comes a Nendoroid of the aircraft carrier, Akagi!', 'Japan', '1', '1', '750000', '28', '9', '25', '2013-12-19', '1');
INSERT INTO `product` VALUES ('15', 'Super Sonico Cheer Girl ver', 'images/Product/15.jpg', 'Sonico chan series that Orchid Seed brings you, the upcoming lineup is [Cheer Girl]!', 'Japan', '6', '5', '2300000', '10', '30', '902', '2013-11-09', '1');
INSERT INTO `product` VALUES ('16', 'Kousaka Kirino [Primary School Ver.] ', 'images/Product/16.jpg', 'Kirino-chan from the popular anime Ore no Imouto ga Konna ni Kawaii Wake ga Nai', 'Japan', '4', '5', '1300000', '58', '20', '678', '2013-08-09', null);
INSERT INTO `product` VALUES ('17', 'Levi', 'images/Product/17.jpg', 'From the anime \'Attack on Titan\' comes a figma of mankind\'s strongest soldier, Levi!', 'Japan', '5', '5', '800000', '123', '20', '124', '2013-09-08', null);
INSERT INTO `product` VALUES ('18', 'Figuarts Zero Cecilia Alcott', 'images/Product/18.jpg', 'Cecilia Alcott from Sci-Fi anime IS: Infinite Stratos', 'Japan', '2', '5', '700000', '70', '10', '891', '2013-09-06', '1');
INSERT INTO `product` VALUES ('19', 'MS Girl Unicorn Gundam', 'images/Product/19.jpg', 'By the design of Mika Akitaka x Hajime Katoki, Makeover to characteristic Destroy Mode and Unicorn Mode were reproduced in part replacement of new interpretation!', 'Japan', '2', '5', '1000000', '40', '10', '876', '2013-12-09', null);
INSERT INTO `product` VALUES ('20', 'Tobiichi Origami', 'images/Product/20.jpg', 'First in the industry, original light novel will come with it! One of main heroine of [date-A-Live] an animation of Spring 2013 [Tobiichi Origami] has appeared in the Armor Girls Project!', 'Japan', '2', '5', '1200000', '90', '20', '901', '2012-12-28', null);
INSERT INTO `product` VALUES ('21', 'Super Sonico -Little Devil Nurse ver', 'images/Product/Super Sonico -Little Devil Nurse ver.jpg', '\r\n- Redesigned from a neat and clean image, re-appeared in black Sexy Nurse!\r\n- Seduces you in translucent stockings and black nurse outfit whice shines white skin.\r\n- Red etching glasses for the nurse outfit also included.\r\n- This glasses can be attached to the normal version so please try.\r\n', 'Japan', '6', '5', '16000000', '20', '30', '0', '2012-12-12', null);
INSERT INTO `product` VALUES ('22', 'Saber Fate EXTRA CCC', 'images/Product/Saber FateEXTRA CCC.jpg', 'Sexy Saber from Fate/EXTRA CCC.', 'Japan', '4', '5', '2000000', '10', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('23', 'Dragons Crown Elf ', 'images/Product/Dragons Crown Elf .jpg', '- 2D graphics overwhelmingly beautiful, production company to a captive game many fans', 'Japan', '3', '5', '1800000', '20', '0', '0', '2014-12-12', '0');
INSERT INTO `product` VALUES ('24', 'Hidamari Sae', 'images/Product/Hidamari Sae.jpg', 'The diligent girl who tries to keep things calm at the Hidamari Apartments.', 'Japan', '5', '5', '700000', '100', '0', '0', '2012-12-12', '0');
INSERT INTO `product` VALUES ('25', 'Nagato Cushion Cover', 'images/Product/Nagato Cushion Cover.jpg', '- Cushion cover printed on both sides Nagato type most Kang battleship [Nagato]!\r\n- You can in the car and room and have a relaxing time with your favorite character.', 'Japan', '8', '3', '460000', '200', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('26', 'Atago Cushion Cover', 'images/Product/Atago Cushion Cover.jpg', '- Cushion cover printed on both sides Kaohsiung type second ship and heavy cruiser the Atago]!\r\n- You can in the car and room and have a relaxing time with your favorite character.', 'Japan', '8', '3', '450000', '200', '0', '0', '2014-12-12', '1');
INSERT INTO `product` VALUES ('27', 'Ecstasy Cushion Cover O', 'images/Product/Ecstasy Cushion Cover O.jpg', '- Ain\'t body got time for that', 'Japan', '8', '3', '250000', '90', '0', '0', '2012-12-12', '0');
INSERT INTO `product` VALUES ('30', 'Machine-Doll Dakimakura  Fray ', 'images/Product/Machine-Doll Dakimakura  Fray .jpg', 'From Machine-Doll anime series. ', 'Japan', '6', '3', '1800000', '15', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('31', 'Evangelion Sticker Set A', 'images/Product/Evangelion Sticker Set A.jpg', '- Evangelion Sticker Set A: Nerv(Gray)', 'Japan', '9', '7', '100000', '1000', '0', '0', '2012-12-12', '0');
INSERT INTO `product` VALUES ('32', 'Evangelion Sticker Set B', 'images/Product/Evangelion Sticker Set B.jpg', '- Evangelion Sticker Set B: Asuka`s Cap ', 'Japan', '9', '7', '100000', '100', '0', '0', '2012-12-12', '0');
INSERT INTO `product` VALUES ('33', 'Evangelion Sticker Set C', 'images/Product/Evangelion Sticker Set C.jpg', '- Evangelion Sticker Set C: Nerv', 'Japan', '9', '7', '100000', '100', '0', '1', '2012-12-12', '0');
INSERT INTO `product` VALUES ('34', 'Evangelion Sticker Set D', 'images/Product/Evangelion Sticker Set D.jpg', 'Evangelion Sticker Set D: Wunder.', 'Japan', '9', '7', '100000', '100', '0', '0', '2013-12-12', '1');
INSERT INTO `product` VALUES ('35', 'Zeon Assault Backpack Black', 'images/Product/Zeon Assault Backpack Black.jpg', '- Military bag of Zion that combines high functionality and facilities.Optimal pocket is enriched, and functional that you can attach a carabiner and porches, and tool\r\n- Install the extension belt! It is the specifications that it is easy to use with a convenient.\r\n- Expansion belt called standard of equipment the U.S. military also has adopted the [MOLLE system] is\r\nRather than just convenience, you can enjoy the original custom to be easy to use!\r\n- Emblem that comes with a removable, or removable emblem series of Cospa, commercially available\r\nCan be installed removable emblem.\r\n- Feel is distinguished Because I use a mesh breathable material in the shoulder belt and back!\r\n- Can play an active part course as everyday use, even survival game and during the outdoor,\r\n-It is a military item full-scale.', 'Japan', '8', '8', '3050000', '20', '0', '0', '2014-12-12', '1');
INSERT INTO `product` VALUES ('36', 'Zaku Spike Armor Bag', 'images/Product/Zaku Spike Armor Bag.jpg', '- 2way +1 way bag that was likened can be called the trademark of [Zaku], the shoulder in the spike armor]!\r\n- The novel is called become as attached to the Zaku in how to use the shoulder of +1 way, which is also the point of maximum', 'Japan', '8', '8', '1050000', '40', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('37', 'Zeon Assault Tote Bag ', 'images/Product/Zeon Assault Tote Bag .jpg', '-  Military tote bag of Zion that combines high functionality and facilities.\r\n\r\n- You may want a perfect pocket, carabiner and porches, and tools to accessory case throughout\r\nInstall the extension belt functional you can! It is the specifications that it is easy to use with a convenient.\r\n- Belt for expansion called standard of equipment the U.S. military also has adopted the [MOLLE system] is\r\nRather than just convenience, you can enjoy the original custom to be easy to use!\r\n- Emblem that comes with a removable, or removable emblem series of Cospa, commercially available\r\nCan be installed removable emblem.\r\n- Belts for shoulders can be mounted removed, apparel and mood of the day,\r\nI can choose how to use your favorite.\r\n- Excellent for storing and surface features in the rugged construction, it is military items full-fledged.', 'Japan', '8', '8', '2000000', '15', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('38', 'Beyond the Boundary Microfiber Sports Towel', 'images/Product/Beyond the Boundary Microfiber Sports Towel.jpg', '', 'Japan', '1', '1', '600000', '100', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('39', 'Bath Towel Rinin', 'images/Product/Bath Towel Rinin.jpg', '', 'Japan', '4', '9', '860000', '100', '30', '0', '2013-12-12', '1');
INSERT INTO `product` VALUES ('42', 'ZGMF-X42S Destiny Gundam', 'images/Product/ZGMF-X42S Destiny Gundam.jpg', '[Mobile Suit Gundam SEED DESTINY]', 'Japan', '2', '2', '440000', '100', '30', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('43', 'MS-06F Zaku II', 'images/Product/MS-06F Zaku II.jpg', '- The production model is here to support the overwhelming forces of Zeon! The long-awaited production appeared Zaku!', 'Japan', '2', '2', '400000', '200', '0', '0', '2014-12-12', '1');
INSERT INTO `product` VALUES ('44', 'MS-07B-3 Gouf Custom ', 'images/Product/MS-07B-3 Gouf Custom .jpg', 'Gundam 08 MS Platoon', 'Japan', '2', '2', '500000', '200', '0', '0', '2014-12-12', '1');
INSERT INTO `product` VALUES ('45', 'Chara X Cushion PETIT IDOLM@STER', 'images/Product/Chara X Cushion PETIT IDOLM@STER.jpg', '- The Masu [petit! Chara × transaction] the second bullet, which is good design joins the summer!', 'Japan', '9', '3', '460000', '200', '0', '0', '2012-12-12', '0');
INSERT INTO `product` VALUES ('46', 'Miyauchi Renge Cushion ', 'images/Product/Miyauchi Renge Cushion .jpg', '-  In the car or indoors, it can spend a relaxing time with your favorite character,\r\nCushion cover printed on both sides [Ren and nine times.\r\n- Can be replaced with a fastener, cushion covers 45 × 45cm is also easy washing.', 'Japan', '9', '3', '460000', '200', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('47', 'Mug Cup Kongo', 'images/Product/Mug Cup Kongo.jpg', '- [- This ship - Fleet Collection popular browser game than, ♪ is the appearance of the mug\r\n- And of Kongo from England reminiscent of tea, and two types of Atago the image of a hot milk!', 'Japan', '5', '4', '200000', '400', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('48', 'Mug Cup Atago', 'images/Product/Mug Cup Atago.jpg', '- [- This ship - Fleet Collection popular browser game than, ♪ is the appearance of the mug\r\n- And of Kongo from England reminiscent of tea, and two types of Atago the image of a hot milk!\r\n', 'Japan', '5', '4', '200000', '300', '0', '0', '2014-12-12', '1');
INSERT INTO `product` VALUES ('49', 'Gingitsune Cup', 'images/Product/Gingitsune Cup.jpg', '- This is a picture of the hull and GinTaro that was used in the ending.\r\nFigure sleeping peacefully is is cute.', 'Japan', '4', '4', '200000', '100', '0', '0', '2014-12-12', '1');
INSERT INTO `product` VALUES ('50', 'Miki Sayaka Messenger Bag', 'images/Product/Miki Sayaka Messenger Bag.jpg', 'Puella Magi Madoka Magica The Movie Part 3: Rebellion Miki Sayaka Messenger Bag', 'Japan', '4', '1', '800000', '200', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('51', ' Sakura Kyoko Messenger Bag', 'images/Product/ Sakura Kyoko Messenger Bag.jpg', 'Puella Magi Madoka Magica The Movie Part 3: Rebellion Sakura Kyoko Messenger Bag ', 'Japan', '4', '8', '800000', '200', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('52', 'Tomoe Mami Messenger Bag', 'images/Product/Tomoe Mami Messenger Bag.jpg', '- Puella Magi Madoka Magica The Movie Part 3: Rebellion Tomoe Mami Messenger Bag', 'Japan', '4', '8', '880000', '200', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('53', 'Akemi Homura Messenger Bag', 'images/Product/Akemi Homura Messenger Bag.jpg', '- Puella Magi Madoka Magica The Movie Part 3: Rebellion Akemi Homura Messenger Bag ', 'Japan', '4', '8', '800000', '300', '0', '0', '2012-12-12', '1');
INSERT INTO `product` VALUES ('54', 'Kaname Madoka Messenger Bag', 'images/Product/Kaname Madoka Messenger Bag.jpg', '- Puella Magi Madoka Magica The Movie Part 3: Rebellion Kaname Madoka Messenger Bag', 'Japan', '4', '8', '800000', '200', '0', '0', '2014-12-12', '1');
INSERT INTO `product` VALUES ('55', 'Elizabeshiba', 'images/Product/Elizabeshiba.jpg', '- Draw give in completely new and drawn Sorachi Hideaki teacher, a curtain pull anime ', 'Japan', '9', '8', '200000', '200', '0', '0', '2014-12-12', '1');

-- ----------------------------
-- Table structure for `type`
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type` (
  `Type_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Type_Name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Type_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of type
-- ----------------------------
INSERT INTO `type` VALUES ('1', 'Nendoroid');
INSERT INTO `type` VALUES ('2', 'GunPla');
INSERT INTO `type` VALUES ('3', 'Cushion');
INSERT INTO `type` VALUES ('4', 'Cup');
INSERT INTO `type` VALUES ('5', 'PVC Figure');
INSERT INTO `type` VALUES ('6', 'Dakimakura');
INSERT INTO `type` VALUES ('7', 'Sticker');
INSERT INTO `type` VALUES ('8', 'Bag');
INSERT INTO `type` VALUES ('9', 'Towel');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `User_ID` int(64) NOT NULL AUTO_INCREMENT,
  `User_FullName` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `User_Name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `User_Pass` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `User_Birthday` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `User_Location` int(64) NOT NULL,
  `User_Admin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`User_ID`),
  UNIQUE KEY `pk_user` (`User_ID`),
  KEY `fk_userlocation` (`User_Location`) USING BTREE,
  CONSTRAINT `fk_user_location` FOREIGN KEY (`User_Location`) REFERENCES `location` (`Location_ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'My Full Name', 'admin', '123456', '2013-12-19', '1', '1');
INSERT INTO `user` VALUES ('2', 'Full Name 2', 'user1', '123456', '2013-12-17', '1', '0');
INSERT INTO `user` VALUES ('3', 'Meh Meh', 'test5', '123456', '4/7/1959', '5', '0');
