<form action="user_script.php" method="post">
<table border="1" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td>Họ Tên</td>
        <td>
            <input type="text" id="fullname" name="fullname" />
        </td>
    </tr>
    <tr>
		<td>Tên đăng nhập</td>
        <td>
            <input type="text" id="username" name="username" />
        </td>  
    </tr>
    <tr>
		<td>Password</td>
        <td>
            <input type="text" id="password" name="password" />
        </td>  
    </tr>
    <tr>
		<td>Ngày sinh</td>
        <td>
            <select class="select-style" id="BirthDay" name="BirthDay">
				<option value="">Ngày</option>
				<?php
					for ($i=1; $i<=31; $i++) {
						echo "<option value=$i>$i</option> \n";
					}
				?>
			</select>
			<select class="select-style" id="BirthMonth" name="BirthMonth">
				<option value="">Tháng</option>
				<?php
					for ($i=1; $i<=12; $i++) {
						echo "<option value=$i>$i</option> \n";
					}
				?>

			</select>
			<select class="select-style" id="BirthYear" name="BirthYear">
				<option value="">Năm</option>
				<?php
					for ($i=1940; $i<=2013; $i++) {
						echo "<option value=$i>$i</option> \n";
					}
				?>
			</select>
        </td>
    </tr>
    <tr>
		<td>Địa chỉ</td>
        <td>
        	<select class="select-style" id="location" name="location">
                <option value="" selected="selected">Tỉnh/Thành</option>
    			<?php
    			$records = DataProvider::ExecuteQuery("select * from location");
    			if($records != false) {
    				while($row = mysql_fetch_array($records, MYSQL_ASSOC)) {
                ?>
                <option value="<?php echo $row["Location_ID"]?>"><?php echo $row["Location_City"];?></option>
                <?php }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <input type="submit" id="submit_add" name="submit_add" value="Thêm" />
            <input type="submit" id="submit_cancel" name="submit_cancel" value="Hủy" />
        </td>
    </tr>	
</table>
</form>
