<?php
session_start();
include_once("dataprovider.php");
$title = "Pedo Command Center";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style/style-index.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/ajax-loader.js"></script>
<script type="text/javascript" src="scripts/jquery-1.10.2.min.js"></script>
<title><?php echo $title ?></title>
<script>
$(document).ready(function(){
    $('.billdetail').find('td').toggle();
    $('.billdetail').find('.billdesc').toggle();
    $('.billheader').click( function(){
         $(this).parents('.billdetail').find('td').toggle();
         $(this).parents('.billdetail').find('.billdesc').toggle();
    });
});
</script>
</head>

<?php
if(!isset($_SESSION["isAdmin"]))
{
	include_once ("blocked.php");
}
else
{
	if ($_SESSION["isAdmin"]==2) {
?>
<body>
    <table id="container" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td width="100%" height="100">
            <?php include_once("A_header.php"); ?>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td id="content" align="right" valign="top"width= "70%">
            <?php
            //Xu ly bien act luc load trang
            if(isset($_REQUEST["act"]) == false){
                $act = 1;
            }
            else{
                $act = $_REQUEST["act"];
            }
            switch($act){
           	case 1:
            	break;
            case 2:
                include("A_left_QLUser.php");
                break;
            case 3:
                include("A_left_QLBrand.php");
                break;
            case 4:
                include("A_left_QLProduct.php");
                break;
            case 5:
                include("A_left_QLType.php");
                break;
            case 6:
                include("A_left_QLBill.php");
                break;
            }?>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td><?php include_once ("footer.php"); ?></td>
    </tr>
   	</table>
    <?php
    	}
    	else
    	{
    		include_once ("login.php");
    	}
    }
    ?>
</body>
</html>
