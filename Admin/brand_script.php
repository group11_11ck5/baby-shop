<?php
include_once("dataprovider.php");
// Upload and Rename File

if (isset($_POST['add']))
{
    if ($_FILES['file']['size']>0)
    {
        $filename = $_FILES["file"]["name"];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file name
        $file_ext = substr($filename, strripos($filename, '.')); // get file ext
        $allowed_file_types = array('.jpg','.jpeg','.gif','.png');
        if (in_array($file_ext,$allowed_file_types))
        {
            $newfilename = $_POST['brandname'] . $file_ext;
            move_uploaded_file($_FILES["file"]["tmp_name"], "../images/Brand/" . $newfilename);
            $name = $_POST['brandname'];
            $desc = $_POST['branddesc'];
            $logo = "images/Brand/".$newfilename;
            $query = DataProvider::ExecuteQuery("INSERT INTO brand (Brand_Name,Brand_Desc,Brand_Logo) VALUES ('".$name."','".$desc."','".$logo."')");
            header( 'Location: index.php?act=3' ) ;
        }
        elseif (empty($file_basename))
        {
            // file selection error
            echo "Please select a file to upload.";
        }
        else
        {
            // file type error
            echo "Only these file types are allowed for upload: " . implode(', ',$allowed_file_types);
            unlink($_FILES["file"]["tmp_name"]);
        }
    }
    else
    {
        $name = $_POST['brandname'];
        $desc = $_POST['branddesc'];
        $logo = "images/none.jpg";
        $query = DataProvider::ExecuteQuery("INSERT INTO brand (Brand_Name,Brand_Desc,Brand_Logo) VALUES ('".$name."','".$desc."','".$logo."')");
        header( 'Location: index.php?act=3' ) ;
    }
}
elseif (isset($_POST['edit']))
{
    if ($_FILES['file']['size']>0)
    {
        $filename = $_FILES["file"]["name"];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file name
        $file_ext = substr($filename, strripos($filename, '.')); // get file ext
        $allowed_file_types = array('.jpg','.jpeg','.gif','.png');
        if (in_array($file_ext,$allowed_file_types))
        {
            $newfilename = $_POST['brandname'] . $file_ext;
            move_uploaded_file($_FILES["file"]["tmp_name"], "../images/Brand/" . $newfilename);
            $id = $_POST['id'];
            $name = $_POST['brandname'];
            $desc = $_POST['branddesc'];
            $logo = "images/Brand/".$newfilename;
            $query = DataProvider::ExecuteQuery("UPDATE brand SET Brand_Name = '".$name."', Brand_Desc = '".$desc."', Brand_Logo = '".$logo."' WHERE Brand_ID ='".$id."'");
            header( 'Location: index.php?act=3' ) ;
        }
        elseif (empty($file_basename))
        {
            // file selection error
            echo "Please select a file to upload.";
        }
        else
        {
            // file type error
            echo "Only these file types are allowed for upload: " . implode(', ',$allowed_file_types);
            unlink($_FILES["file"]["tmp_name"]);
        }
    }
    else
    {
        $id = $_POST['id'];
        $name = $_POST['brandname'];
        $desc = $_POST['branddesc'];
        $query = DataProvider::ExecuteQuery("UPDATE brand SET Brand_Name = '".$name."', Brand_Desc = '".$desc."' WHERE Brand_ID ='".$id."'");
        header( 'Location: index.php?act=3' ) ;
    }
}
?>
