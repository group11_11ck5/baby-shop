<?php
include_once("dataprovider.php");
// Upload and Rename File

		$name = $_POST['product_name'];
        $desc = $_POST['product_desc'];
		$made = $_POST['product_made'];
		$brand = $_POST['product_brand'];
		$type = $_POST['product_type'];
		$price = $_POST['product_price'];
		$stock = $_POST['product_stock'];
		$hot = $_POST['product_hot'];
		
if (isset($_POST['add']))
{
    if ($_FILES['file']['size']>0)
    {
        $filename = $_FILES["file"]["name"];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file name
        $file_ext = substr($filename, strripos($filename, '.')); // get file ext
        $allowed_file_types = array('.jpg','.jpeg','.gif','.png');
        if (in_array($file_ext,$allowed_file_types))
        {
            $newfilename = $_POST['product_name'] . $file_ext;
            move_uploaded_file($_FILES["file"]["tmp_name"], "../images/Product/" . $newfilename);
            $logo = "images/Product/".$newfilename;
		
            $query = DataProvider::ExecuteQuery("INSERT INTO product (Product_Name,Product_Photo,Product_Desc,Product_MadeIn,Product_Brand,Product_Type,Product_Price,Product_Stock,Product_Hot) VALUES ('".$name."','".$logo."','".$desc."','".$made."','".$brand."','".$type."','".$price."','".$stock."','".$hot."')");
            header( 'Location: index.php?act=4' ) ;
        }
        elseif (empty($file_basename))
        {
            // file selection error
            echo "Please select a file to upload.";
        }
        else
        {
            // file type error
            echo "Only these file types are allowed for upload: " . implode(', ',$allowed_file_types);
            unlink($_FILES["file"]["tmp_name"]);
        }
    }
    else
    {
        $logo = "images/none.jpg";
		$query = DataProvider::ExecuteQuery("INSERT INTO product (Product_Name,Product_Photo,Product_Desc,Product_MadeIn,Product_Brand,Product_Type,Product_Price,Product_Stock,Product_Hot) VALUES ('".$name."','".$logo."','".$desc."','".$made."','".$brand."','".$type."','".$price."','".$stock."','".$hot."')");
            header( 'Location: index.php?act=4' ) ;
    }
}
elseif (isset($_POST['edit']))
{
    if ($_FILES['file']['size']>0)
    {
        $filename = $_FILES["file"]["name"];
        $file_basename = substr($filename, 0, strripos($filename, '.')); // get file name
        $file_ext = substr($filename, strripos($filename, '.')); // get file ext
        $allowed_file_types = array('.jpg','.jpeg','.gif','.png');
        if (in_array($file_ext,$allowed_file_types))
        {
            $newfilename = $_POST['product_name'] . $file_ext;
            move_uploaded_file($_FILES["file"]["tmp_name"], "../images/Product/" . $newfilename);
            $id = $_POST['id'];
            $logo = "images/Product/".$newfilename;
		    $query = DataProvider::ExecuteQuery("UPDATE product SET Product_Name = '".$name."', Product_Desc = '".$desc."', Product_Photo = '".$logo."',Product_MadeIn = '".$made."',Product_Brand = '".$brand."',Product_Type = '".$type."', Product_Stock = '".$stock."', Product_Hot = '".$hot."', Product_Price = '".$price."' WHERE Product_ID ='".$id."'");
            header( 'Location: index.php?act=4' ) ;
        }
        elseif (empty($file_basename))
        {
            // file selection error
            echo "Please select a file to upload.";
        }
        else
        {
            // file type erro
            echo "Only these file types are allowed for upload: " . implode(', ',$allowed_file_types);
            unlink($_FILES["file"]["tmp_name"]);
        }
    }
    else
    {
        $id = $_POST['id'];
        
        $query = DataProvider::ExecuteQuery("UPDATE product SET Product_Name = '".$name."', Product_Desc = '".$desc."',Product_MadeIn = '".$made."',Product_Brand = '".$brand."',Product_Type = '".$type."', Product_Stock = '".$stock."', Product_Hot = '".$hot."', Product_Price = '".$price."' WHERE Product_ID ='".$id."'");
        header( 'Location: index.php?act=4' ) ;
    }
}
?>