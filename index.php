<?php
    include_once("dataprovider.php");
    include_once("incpanel.php");
   	include_once("incproduct.php");
    include_once("incmenu.php");
    include_once("incad.php");
    include_once("timkiemnhanh.php");
    include_once("class/cart.php");
    include_once("class/product.php");
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style/style_index.css" rel="stylesheet" type="text/css" />
        <link href="style/style_header.css" rel="stylesheet" type="text/css" />
        <link href="style/style_login.css" rel="stylesheet" type="text/css" />
        <link href="style/style_registryform.css" rel="stylesheet" type="text/css" />
        <link href="style/style_item.css" rel="stylesheet" type="text/css" />
        <link href="style/style_styledpanel.css" rel="stylesheet" type="text/css" />
        <link href="style/style_column.css" rel="stylesheet" type="text/css" />
        <link href="style/style_smoothDivScroll.css" rel="stylesheet" type="text/css" />
        <link href="style/style_fancybox.css" rel="stylesheet" type="text/css" />
        <link href="style/style_itemdetail.css" rel="stylesheet" type="text/css" />
        <link href="style/style_buybutton.css" rel="stylesheet" type="text/css" />
        <link href="style/style_footer.css" rel="stylesheet" type="text/css" />
        <link href="style/style_css-tooltips.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="scripts/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="scripts/jquery.imgtooltip.js"></script>
        <script type="text/javascript" src="scripts/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="scripts/jquery.leanModal.min.js"></script>
        <script type="text/javascript" src="scripts/jquery.notify.min.js"></script>
        <script type="text/javascript" src="scripts/ajax-loader.js"></script>

        <title>Baby shop</title>

    </head>
    <body>
     <script type="text/javascript">
        $(document).ready(function(){
            $('[ref=addgiohang]').click(function(){
                ID = $(this).attr("id");
                $.post("addgiohang.php",{productid:ID},function( data ){
                    if (data==0) {
                        $.notify("Bạn cần phải đăng nhập để mua hàng.", "warn");
                    } else{
                        $.notify("Đã thêm hàng vào giỏ.", "success");
                    };
                    });
            });
            $('.outStockButton').click(function(){
                $.notify("Sản phẩm đã hết hàng.", "warn")
            });
        });
        </script>
    <div id="container">
        <div id="header">
            <?php include_once("incheader.php") ?>
        </div>
		<div id="signup-modal" style="display:none;" >
			<?php include_once("registry-form.php") ?>
		</div>
        <div>
            <?php StyledPanel(".:: Sản Phẩm HOT ::.","ReturnProduct",array(4,2)); ?>
        </div>
        <div id="content">
            <div id="mid" class="column">
<?php
                //Xu ly bien act luc load trang
                if(isset($_REQUEST["act"]) == false){
                    $act = 6;
                }
                else{
                    $act = $_REQUEST["act"];
                }
                switch($act){
                    case 1:
                        StyledPanel(".:: Sản Phẩm ::.","ReturnProduct",array(1,1));
                        break;
                    case 2:
                        StyledPanelN(".:: Phương Thức Thanh Toán ::.","incshipment.php");
                        break;
                    case 3:
                        StyledPanelN(".:: Chi Tiết Liên Hệ ::.","inccontact.php");
                        break;
                    case 4:
                        StyledPanelN(".:: Giỏ Hàng ::.","inccart.php");
                        break;
                    case 5:
                        StyledPanel("Tìm với từ khóa \"".$_GET["q"]."\"","FastSearch",array($_GET["q"]));
                        break;
                    case 6:
                        StyledPanel(".:: Sản Phẩm Bán Chạy ::.","ReturnProduct",array(2,1));
						StyledPanel(".:: Sản Phẩm Mới ::.","ReturnProduct",array(3,1));
                        break;
                    case 7:
                        StyledPanelN(".:: Tìm Kiếm Nâng Cao ::.","timkiemNC.php");
                        break;
                    case 8:
                        StyledPanelN(".:: Thanh Toán ::.","checkout.php");
                        break;
                    case 9:
                        StyledPanelN(".:: Chi Tiết Item ::.","incdetailfull.php");
                        break;
                    case 10:
                        StyledPanelN(".:: Hóa Đơn ::.","incbill.php");
                        break;
                    }
?>
            </div>
            <div id="left" class="column">
                <?php StyledPanel("Doanh Mục Hàng","ReturnMenu",array(1)) ?>

                <?php StyledPanel("Nhà Sản Xuất","ReturnMenu",array(2)) ?>
            </div>
            <div id="right" class="column">
                <?php StyledPanel("Ads","ReturnAd",array(0)) ?>
            </div>
        </div>
        <div id="footer">
            <?php include_once("incfooter.php") ?>
        </div>
    </div>
    </body>
</html>
