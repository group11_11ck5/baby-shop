<?php
class Product{
    var $ProductID;
    var $ProductName;
    var $ProductPrice;
    var $Quantity;
    var $Sum;

    public function __construct($pProductID, $pProductName, $pQuantity, $pProductPrice){
        $this->ProductID = $pProductID;
        $this->ProductName = $pProductName;
        $this->ProductPrice = $pProductPrice;
        $this->Quantity = $pQuantity;
        $this->Sum = $this->ProductPrice * $this->Quantity;
    }

    public function getProductID(){
        return $this->ProductID;
    }

    public function getProductName(){
        return $this->ProductName;
    }

    public function getProductPrice(){
        return $this->ProductPrice;
    }

    public function getQuantity(){
        return $this->Quantity;
    }

    public function setQuantity($value){
        $this->Quantity = $value;
    }

    public function getSum(){
        return $this->Sum;
    }

    public function setSum($value){
        $this->Sum = $value;
    }
}
?>
