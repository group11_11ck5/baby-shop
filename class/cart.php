<?php
class Cart {
    var $CustomerID;
    var $ProductList;

    public function __construct($pCustomerID){
        $this->CustomerID = $pCustomerID;
        $this->ProductList = array();
    }

    public function getCustomerID(){
        return $this->CustomerID;
    }

    public function setCustomerID($value){
        $this->CustomerID = $value;
    }

    public function getProductList(){
        return $this->ProductList;
    }

    public function IsProductSelected($pProductID){
        $res = 0;
        foreach ($this->ProductList as $value){
            if($value->getProductID() == $pProductID){
                $res = $value->Quantity;
            }
        }
        return $res;
    }

    public function UpdateProduct($pProductID, $pQuantity){
        foreach ($this->ProductList as &$value){
            if($value->getProductID() == $pProductID){
                $value->setQuantity($value->getQuantity() + $pQuantity);
                $value->setSum($value->getSum() + $value->ProductPrice * $pQuantity);
                return;
            }
        }
    }

    public function InsertProduct($pProductID, $pProductName, $pQuantity, $pProductPrice){
        $q = $this->IsProductSelected($pProductID);
        if($q==0){
            $this->ProductList[]  = new Product($pProductID, $pProductName, $pQuantity, $pProductPrice);
        }
        else{
            $this->UpdateProduct($pProductID, $pQuantity);
        }
    }

    public function EditProduct($pProductID, $pQuantity){
        foreach ($this->ProductList as &$value){
            if($value->getProductID() == $pProductID){
                $value->setQuantity($pQuantity);
                $value->setSum($value->ProductPrice * $pQuantity);
                return;
            }
        }
    }

    public function DeleteProduct($pProductID){
        $this->ProductList = array_values($this->ProductList);
        for ($i = 0 ; $i < count($this->ProductList) ; $i++){
            if($this->ProductList[$i]->getProductID() == $pProductID){
                unset($this->ProductList[$i]);
                return;
            }
        }
    }

    public function SumProduct(){
        $res = 0;
        foreach ($this->ProductList as $value){
            $res += $value->Quantity;
        }
        return $res;
    }

    public function SumMoney(){
        $res = 0;
        foreach ($this->ProductList as $value){
            $res += $value->Sum;
        }
        return $res;
    }
}
?>
