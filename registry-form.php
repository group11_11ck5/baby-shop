﻿<div class="registry-form">
<form id="signup-form" name="signup-form" method="POST" action="">
	<div style="height: 18px; padding: 5px 16px; margin-bottom: 8px; background: #4b8df9; color: white; line-height: 1em; font-size: 15px; align: center;">
		<span>Đăng ký tài khoản</span>
	</div>
	<fieldset>
		<legend>Thông tin cá nhân</legend>
		<fieldset>
			<label for="fullname" style="margin-right: 3px;">Họ tên</label>
			<input id="fullname" name="fullname" type="text" style="width: 200px;" title="Họ tên phải có tối thiểu 6 ký tự"/>
			<span class="warning nameFail"><img src="images/logos/error.png" /></span>
		</fieldset>
		<fieldset>
			<label class="day" >Ngày sinh
			<select class="select-style" id="BirthDay" name="BirthDay">
				<option value="">Ngày</option>
				<?php
					for ($i=1; $i<=31; $i++) {
						echo "<option value=$i>$i</option> \n";
					}
				?>
			</select>
			</label>
			<label class="month">
			<select class="select-style" id="BirthMonth" name="BirthMonth">
				<option value="">Tháng</option>
				<?php
					for ($i=1; $i<=12; $i++) {
						echo "<option value=$i>$i</option> \n";
					}
				?>

			</select>
			</label>
			<label class="year">
			<select class="select-style" id="BirthYear" name="BirthYear">
				<option value="">Năm</option>
				<?php
					for ($i=1940; $i<=2013; $i++) {
						echo "<option value=$i>$i</option> \n";
					}
				?>
			</select>
			</label>
			<span class="warning dateFail"><img src="images/logos/error.png" /> </span>
		</fieldset>
		<fieldset>
			<label class="city"><span style="margin-right: 4px;">Địa điểm</span>
			<select class="select-style" id="location" name="location">
                <option value="" selected="selected">Tỉnh/Thành</option>
				<?php
				$records = DataProvider::ExecuteQuery("select * from location");
				if($records != false) {
					while($row = mysql_fetch_array($records, MYSQL_ASSOC)) {
                ?>
                <option value="<?php echo $row["Location_ID"]?>"><?php echo $row["Location_City"];?></option>
                <?php }
                }
                ?>

			</select>
			</label>
			<span class="warning locationFail"><img src="images/logos/error.png" /></span>
		</fieldset>
	</fieldset>
	<fieldset>
		<legend>Thông tin tài khoản</legend>
		<fieldset>
			<label for="name">Tên đăng nhập</label>
			<input type="text" id="name" name="name" title="Tên đăng nhập phải có tối thiểu 5 ký tự" />
			<span class="warning usernameFail"><img src="images/logos/error.png" /></span>
			<span class="warning usernameSuccess"><img src="images/logos/success.png" /></span>
		</fieldset>
		<fieldset>
			<label for="password" style="margin-right: 44px;">Mật khẩu</label>
			<input type="password" id="pass_main" name="password" title="Mật khẩu phải có ít nhất 5 ký tự" />
			<span class="warning passFail"><img src="images/logos/error.png" /></span>
		</fieldset>
		<fieldset>
			<label for="pass_temp">Nhập lại mật mã</label>
			<input type="password" id="pass_temp" name="pass_temp"/>
			<span class="warning passFail"><img src="images/logos/error.png" /></span>
		</fieldset>
		<fieldset>
			<div style="margin: 0px 0px 16px 120px;">
				<img src="captcha.php" id="img_captcha" />
				<img src="images/logos/change.png" id="load_captcha" style="cursor: pointer;" />
			</div>
			<label for="captcha" style="margin-right: 8px;">Nhập mã trên</label>
			<input type="text" id="captcha" name="captcha" title="Phải nhập đúng captcha mới có thể đăng ký!" />
			<span class="warning captchaFail"><img src="images/logos/error.png" /></span>
		</fieldset>
			<span style="font-size: 10px; color: red; margin-left: 16px;">Lưu ý: Nếu không có thông báo đăng ký thành công vui lòng thử lại.</span>
	</fieldset>
	<div style="padding: 0px 130px;">
		<input class="buttom" name="bt_signup" id="bt_signup" tabindex="5" value="Đăng ký" type="submit" />
	</div>
	<?php
		error_reporting(~E_ALL);
		if (isset($_POST['bt_signup'])) {
			$uid = uniqid();
		}
	?>
	<!-- user id -->
	<input id="uid" name="uid" style="display:none;"  value="<?php echo $uid; ?>" />
</form>
</div>
<!-- jquery script leanmodal -->
<script type="text/javascript">
		$(function(){
			$('#signup-trigger').leanModal({ top: 70, overlay: 0.45, closeButton: ".hidemodal" });
		});
</script>

<?php
	include_once("dataprovider.php");
	//session_start();
	$flag = 0;
	if($_SESSION['check_code'] == $_POST['captcha']) {
		if(isset($_POST['bt_signup']))	{
			$flag = 1;
			$uid = $_POST['uid'];
			$fullname = $_POST['fullname'];
			$birth = $_POST['BirthDay'].'/'.$_POST['BirthMonth'].'/'.$_POST['BirthYear'] ;
			$location = $_POST['location'];
			$name = $_POST['name'];
			$password = $_POST['password'];
			//$hash_pass = sha1($password);
			$query = "INSERT INTO user (User_FullName,User_Name,User_Pass,User_Birthday,User_Location,User_Admin) VALUES ('".$fullname."','".$name."','".$password."','".$birth."','".$location."','0')";
			DataProvider::ExecuteQuery($query);
		}
	}
	if ($flag == 1)
		echo '<script>alert("Đăng ký thành công! \nMời bạn đăng nhập tài khoản.");</script>"';
?>
