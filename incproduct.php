<?php
function ReturnProduct($type,$display)
{
    /*
    type: lấy các loại product, có 3 loại
    1 - lấy tất cả product
    2 - lấy 10 product bán chạy
    3 - lấy 10 product mới nhất
	4 - lấy 10 product HOT
    display: cách hiển thị
    1 - đầy đủ, không có smoothscrollDiv
    2 - rút gọn, có smoothscrollDiv, dùng hiển thị sidebar
    */
    if ($type == 1) {
         $result = dataprovider::ExecuteQuery("select b.*, c.Type_Name, p.Brand_Name from ((Product b inner join Brand p on p.Brand_ID = b.Product_Brand) inner join Type c on c.Type_ID = b.Product_Type)");
    }
    elseif ($type == 2) {
        $result = dataprovider::ExecuteQuery("select * from ((Product b inner join Brand p on p.Brand_ID = b.Product_Brand) inner join Type c on c.Type_ID = b.Product_Type) ORDER BY Product_Sell DESC LIMIT 12") ;
    }
    elseif ($type == 3 ) {
        $result = dataprovider::ExecuteQuery("select * from ((Product b inner join Brand p on p.Brand_ID = b.Product_Brand) inner join Type c on c.Type_ID = b.Product_Type) ORDER BY Product_Date DESC LIMIT 12") ;
    }
	 elseif ($type == 4 ) {
        $result = dataprovider::ExecuteQuery("select * from ((Product b inner join Brand p on p.Brand_ID = b.Product_Brand) inner join Type c on c.Type_ID = b.Product_Type) where b.Product_Hot='1'") ;
    }
    if ($display == 1) {
    	// Xử lý request brandid nếu có
    	if(isset($_REQUEST["brandid"])){
			$result = dataprovider::ExecuteQuery("select * from ((Product b inner join Brand p on p.Brand_ID = b.Product_Brand) inner join Type c on c.Type_ID = b.Product_Type) where b.Product_Brand=".$_REQUEST["brandid"]);
		}
		// Xử lý request typeid nếu có
		if(isset($_REQUEST["typeid"])){
			$result = dataprovider::ExecuteQuery("select * from ((Product b inner join Brand p on p.Brand_ID = b.Product_Brand) inner join Type c on c.Type_ID = b.Product_Type) where b.Product_Type=".$_REQUEST["typeid"]);
		}
        while ($row = mysql_fetch_array($result,MYSQL_ASSOC))
        {
        ?>
            <div class="item">
            <!--<div class="itemImg"> <a href="<?php echo $row["Product_Photo"];?>" class="preview"><img src="<?php echo $row["Product_Photo"];?>"></a> </div>-->
            <div class="itemImg"> <a href="incdetail.php?productid=<?php echo $row["Product_ID"] ?>" class="fancybox fancybox.ajax"><img src="<?php echo $row["Product_Photo"];?>"></a> </div>
            <a href="incdetail.php?productid=<?php echo $row["Product_ID"] ?>" class="fancybox fancybox.ajax" data-tip="<?php echo $row["Product_Name"]; ?>">
            <h4><?php echo $row["Product_Name"]; ?> </h4>
            </a>
            <div class="footer <?php
              if ($row["Product_Sell"] < $row["Product_Stock"])
              {
                echo "in-stock";
              }
              else
              {
                echo "out-stock";
              }
          ?>">
              <div class="itemPrice"> <?php echo number_format($row["Product_Price"]) ?> VND </div>
            </div>
            <div class="buyBtn">

              <span class="<?php
              if ($row["Product_Sell"] < $row["Product_Stock"])
              {
                echo "buyButton\" ref=\"addgiohang\"";
              }
              else
              {
                echo "outStockButton\"";
              }
              ?> id="<?php echo $row["Product_ID"] ?>">
              <?php if ($row["Product_Sell"] < $row["Product_Stock"])
              {
                echo "Mua";
              }
              else
              {
                echo "Hết hàng";
              }
              ?>
            </span>
            </div>
          </div>
<?php
        }
                        }
    else {
?>
<div id="scrollingText">
<?php
while ($row = mysql_fetch_array($result,MYSQL_ASSOC))
{
 ?>
    <div class="miniitem">
        <a href="incdetail.php?productid=<?php echo $row["Product_ID"] ?>" class="fancybox fancybox.ajax">
        <img src="<?php echo $row["Product_Photo"];?>">
        <h4><?php echo $row["Product_Name"]; ?></h4>
    </a>
    </div>
    <?php
}
?>
</div>

    <!-- LOAD JAVASCRIPT LATE - JUST BEFORE THE BODY TAG
         That way the browser will have loaded the images
         and will know the width of the images. No need to
         specify the width in the CSS or inline. -->

    <!-- jQuery UI Widget and Effects Core (custom download)
         You can make your own at: http://jqueryui.com/download -->
    <script src="scripts/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>

    <!-- Latest version of jQuery Mouse Wheel by Brandon Aaron
         You will find it here: http://brandonaaron.net/code/mousewheel/demos -->
    <script src="scripts/jquery.mousewheel.min.js" type="text/javascript"></script>

    <!-- jQuery Kinetic - for touch -->
    <script src="scripts/jquery.kinetic.min.js" type="text/javascript"></script>

    <!-- Smooth Div Scroll 1.3 minified -->
    <script src="scripts/jquery.smoothdivscroll-1.3-min.js" type="text/javascript"></script>

    <!-- If you want to look at the uncompressed version you find it at
         js/source/jquery.smoothDivScroll-1.3.js -->

    <!-- Plugin initialization -->
    <script type="text/javascript">
        $(document).ready(function () {
            $("#scrollingText").smoothDivScroll({
                mousewheelScrolling: "allDirections",
                manualContinuousScrolling: true,
                autoScrollingMode: ""
            });
        });
</script>
<?php
    }
?>
    <script>
        $(document).ready(function() {
            $('.fancybox').fancybox({
                padding : 5,
                openEffect  : 'elastic'
            });
        });
    </script>
<?php
}
?>
